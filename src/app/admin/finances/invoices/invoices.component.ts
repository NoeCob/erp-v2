import { Component, OnInit } from '@angular/core';
import { InvoicesService } from '../../../services/invoices.service';
import { BroadcastService } from '../../../services/broadcast.service';
import { Subscription } from 'rxjs';
import { ModalComponent } from '../../../components/modal/modal.component';
import { SwalService } from '../../../services/swal.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {
  baseUrl = `${environment.apiUrl}`;
  message: any;
  dataTableConfig = {
    config: {
      base: this.invoices,
      api: 'getInvoices',
      order: [[0, 'desc']]
    },
    columns: [
      {
        display: 'ID',
        field: 'id_invoices',
        type: 'text'
      },
      {
        display: 'Cliente',
        field: 'client.name',
        type: 'text'
      },
      {
        display: 'RFC',
        field: 'client.rfc',
        type: 'text'
      },
      {
        display: 'Fecha',
        field: 'created_at',
        type: 'date'
      },
      {
        display: 'Acciones',
        field: '',
        type: 'inline-button',
        options: [
          {
            cssClass: 'btn btn-primary',
            icon: 'fa fa-fw fa-file-pdf-o',
            event: 'download.pdf',
            conditionality: 'true'
          },
          {
            cssClass: 'btn btn-primary',
            icon: 'fa fa-fw fa-file-code-o',
            event: 'download.xml',
            conditionality: 'true'
          },
          {
            cssClass: 'btn btn-primary',
            icon: 'fa fa-fw fa-download',
            event: 'download.zip',
            conditionality: 'true'
          },
          {
            cssClass: 'btn btn-primary',
            icon: 'fa fa-fw fa-envelope',
            event: 'send.invoice',
            conditionality: 'true'
          }
        ]
      }
    ]
  };

  broadcast$: Subscription;

  constructor(
    private readonly invoices: InvoicesService,
    private readonly broadcast: BroadcastService,
    public appModal: ModalComponent,
    private readonly swal: SwalService,
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch (event.name) {
        case 'download.pdf': this.downloadPdf(event.data); break;
        case 'download.xml': this.downloadXml(event.data); break;
        case 'download.zip': this.downloadZip(event.data); break;
        case 'send.invoice': this.sendInvoice(event.data); break;
      }
    });
  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }

  downloadPdf(data): void {
    window.open(`${this.baseUrl}/invoices_files/${data.filename}.pdf`, '_blank');
    // document.location.href = `${this.baseUrl}/invoices_files/${data.filename}.pdf`;
  }
  downloadXml(data): void {
    window.open(`${this.baseUrl}/invoices_files/${data.filename}.xml`, '_blank');
    // document.location.href = `${this.baseUrl}/invoices_files/${data.filename}.xml`;
  }
  downloadZip(data): void {
    window.open(`${this.baseUrl}/invoices/download_files/${data.id_invoices}`, '_blank');
  }
  sendInvoice(data): void {
    const swalParams = {
      title: 'Envio de Factura',
      text: 'Proporciona el mensaje al cliente',
      inputPlaceholder: `Factura de ${data.name}`
    };
    this.swal.input(swalParams).then((resp: any) => {
      if (resp.value) {
        const params = {
          id_invoices: data.id_invoices,
          message: resp.value
        };
        this.invoices.sendInvoice(params).subscribe((subresp: any) => {
            this.swal.success().then(() => {
              this.activeModal.dismiss();
              this.broadcast.reloadDataTable();
            });
        });
      }
    }).catch();
  }
}
