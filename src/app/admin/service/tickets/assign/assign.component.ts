import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { SwalService } from '../../../../services/swal.service';
import { BroadcastService } from '../../../../services/broadcast.service';
import { TicketsService } from '../../../../services/tickets.service';
import { UsersService } from '../../../../services/users.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'app-assign',
  templateUrl: './assign.component.html',
  styleUrls: ['./assign.component.scss']
})
export class AssignComponent implements OnInit, OnDestroy {
  @Input() data: any;
  message: any;
  baseUrl = `${environment.apiUrl}`;
  image_profile: any;
  dataTableConfig = {
    config: {
      type: 'tickets-technitians',
      base: this.tickets,
      api: 'getTechnicians',
      order: [[0, 'desc']],
      params: {
        status: 1
      }
    },
    columns: [
      {
        display: 'Nombre',
        field: 'name',
        type: 'text'
      },
      {
        display: 'Correo',
        field: 'email',
        type: 'text',
        orderable: false
      },
      {
        display: 'Teléfono',
        field: 'phone',
        type: 'text',
        orderable: false
      },
      {
        display: 'Fecha de alta',
        field: 'created_at',
        type: 'date'
      }
    ],
    filters: [
      {
        cssClass: 'd-none',
        type: 'status',
        options: []
      }
    ]
  };

  broadcast$: Subscription;

  constructor(
    private readonly broadcast: BroadcastService,
    public activeModal: NgbActiveModal,
    private readonly tickets: TicketsService,
    private readonly userService: UsersService,
    private readonly swal: SwalService
  ) { }

  ngOnInit(): void {
    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch (event.name) {
        case 'tickets-technitians-row-click': this.message = event.data; break;
      }
    });
  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }

  assignTechnician(): void {
    const swalParams = {
      title: 'Instrucciones',
      text: 'Proporciona más detalles al técnico para un servico más rápido y eficiente.',
      inputPlaceholder: 'Instrucciones'
    };

    this.swal.input(swalParams).then((resp: any) => {
      if (resp.value) {
        const params = {
          id_technicians: this.message.id,
          details: resp.value,
          status: 'assigned'
        };
        this.tickets.getStatus(params, this.data.id_tickets).subscribe((subresp: any) => {
          if (subresp.success) {
            this.swal.success().then(() => {
              this.activeModal.dismiss();
              if ('isMap' in this.data) {
                this.broadcast.fire({
                  name: 'reload-map',
                  data: {}
                });
              } else {
                this.broadcast.reloadDataTable();
              }
            });
          } else {
            this.swal.error().catch();
          }
        });

      }
    }).catch();
  }

  getImageProfile(message): void {
    this.userService.getImageProfile(message).subscribe((resp: any) => {
      if (resp) {
        this.image_profile = `${this.baseUrl}/profiles/${resp.response}`;
        
        return this.image_profile;
      }
    });
  }
}
