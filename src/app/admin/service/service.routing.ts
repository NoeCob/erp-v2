import { Routes } from '@angular/router';
import { TicketsComponent } from './tickets/tickets.component';
import { ErrorCodesComponent } from './error-codes/error-codes.component';
import { TicketsMapComponent } from './tickets-map/tickets-map.component';

export const ServiceRoutes: Routes = [
    {
        path: 'tickets', 
        component: TicketsComponent
    },
    {
        path: 'codigos_de_error', 
        component: ErrorCodesComponent
    },
    {
        path: 'mapa_de_tickets', 
        component: TicketsMapComponent
    }
];
