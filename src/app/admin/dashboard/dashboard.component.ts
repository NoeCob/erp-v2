import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../services/dashboard.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { FromService } from '../../providers/form.service';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { SwalService } from '../../services/swal.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {

  kpiData = {
    active_users: 0,
    debts: 0,
    free_water: 0,
    free_water_percent: 0,
    sr: 0
  };

  tableData = {
    clientStatus: 0,
    ticketStatus: {
      opened: 0,
      closed: 0,
      assigned: 0,
      confirmed: 0,
      completed: 0
    }
  };

  subscriptionsData = {
    actualSubscriptions: [],
    pastSubscriptions: []
  };

  incomesData = {
    actualIncomes: [],
    pastIncomes: []
  };

  sharesTotalData = {
    total: []
  };

  sharesTypeData = {
    type: []
  };

  cancellationsData = {
    actualCancelations: []
  };

  form: FormGroup = this.formBuilder.group({
    from: [],
    to: []
  });

  constructor(
    private readonly dashboardService: DashboardService,
    private readonly fromService: FromService,
    private readonly formBuilder: FormBuilder,
    private readonly dateFormater: NgbDateParserFormatter,
    private readonly swal: SwalService
  ) { }

  ngOnInit(): void {
    this.getActiveUsers();
    this.getIncomes();
    this.getSubcriptions();
    this.getTickets();
    this.getTotals();
    this.getShares();
    this.fromService.setForm(this.form);

  }

  freeWaterPercent(): number {
    let free_water_percent = 0;
    if (this.kpiData.free_water || this.kpiData.active_users > 0) {
      free_water_percent = Math.round((this.kpiData.free_water / this.kpiData.active_users) * 100);
    }

    return free_water_percent;
  }

  getActiveUsers(): void {
    this.dashboardService.activeUsers().subscribe((resp: any) => {
      resp.clients.forEach((clienStatus: any) => {
        if (clienStatus.status === 'accepted') {
          this.kpiData.active_users = Number(clienStatus.charged);
        }
      });
      
      this.tableData.clientStatus = resp.clients;

      this.dashboardService.freewater().subscribe((data: any) => {
        this.kpiData.free_water = data.length;
        this.kpiData.free_water_percent = this.freeWaterPercent();
      });
    });
  }

  getIncomes(params?): void {
    this.dashboardService.incomes(params).subscribe((resp: any) => {
      const nIn = {
        actualIncomes: [...resp.incomes],
        pastIncomes: [...resp.past_incomes]
      };
      this.incomesData = { ...nIn };
    });
  }

  getShares(params?): void {
    this.dashboardService.shares(params).subscribe((resp: any) => {
      const ntotal = {
        total: [...resp.total]
      };
      const nType = {
        type: [...resp.byType]
      };

      this.sharesTotalData = { ...ntotal };
      this.sharesTypeData = { ...nType };
    });
  }

  getSubcriptions(params?): void {
    this.dashboardService.subscriptions(params).subscribe((resp: any) => {
      const nSubs = {
        actualSubscriptions: [...resp.new],
        pastSubscriptions: [...resp.past]
      };
      this.subscriptionsData = { ...nSubs };
    });
  }

  getTickets(): void {
    this.dashboardService.dashboard().subscribe((resp: any) => {
      this.tableData.ticketStatus.opened = resp.tickets.opened;
      this.tableData.ticketStatus.closed = resp.tickets.closed;
      this.tableData.ticketStatus.assigned = resp.tickets.assigned;
      this.tableData.ticketStatus.confirmed = resp.tickets.confirmed;
      this.tableData.ticketStatus.completed = resp.tickets.completed;

      const nIn = {
        actualCancelations: [...resp.subscription_events]
      };
      this.cancellationsData = { ...nIn };
    });
  }

  getTotals(): void {
    this.dashboardService.totals().subscribe((resp: any) => {
      this.kpiData.debts = resp.totals.debts;
      const clientSocial = parseInt(resp.totals.client_social, 10);
      const aguagenteSocial = parseInt(resp.totals.aguagente_social, 10);
      this.kpiData.sr = Math.floor(clientSocial + aguagenteSocial);
    });
  }

  search(): void {
    const from_sub = this.dateFormater.format(this.form.get('from').value);
    const to_sub = this.dateFormater.format(this.form.get('to').value);
    const params = {
      from: from_sub,
      to: to_sub
    };

    if (params.from && params.to !== '') {
      this.getSubcriptions(params);
      this.getIncomes(params);
      // this.getTickets();
      this.getShares(params);
    } else {
      this.swal.error({ title: 'Favor de introducir las fechas correctamente' });
    }
  }

}
