import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ModalComponent } from '../../../components/modal/modal.component';
import { Subscription } from 'rxjs';
import { BroadcastService } from '../../../services/broadcast.service';
import { ReviewRefundsComponent } from './review-refunds/review-refunds.component';
import { SharedComponent } from '../../../model/shared-component';
import { MakeRefundComponent } from './make-refund/make-refund.component';
import { ChargesService } from '../../../services/charges.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-client-charges-history',
  templateUrl: './client-charges-history.component.html',
  styleUrls: ['./client-charges-history.component.scss']
})
export class ClientChargesHistoryComponent implements OnInit, OnDestroy {
  @Input() data: any;
  dataTableConfig = {
    config: {
      base: this.chargeService,
      api: 'getClientCharges',
      params: {
        id_clients: 0
      }
    },
    columns: [
      {
        display: 'Descripción',
        field: 'description',
        type: 'text'
      },
      {
        display: 'Nota de credito',
        field: 'invoice_status',
        type: 'text'
      },
      {
        display: 'Cantidad',
        field: 'amount',
        type: 'charge_amount'
      },
      {
        display: 'Fecha de pago',
        field: 'paid_at',
        type: 'text'
      },
      {
        display: 'Efectuado en',
        field: 'vendor',
        type: 'text'
      },
      {
        display: 'Tipo de cargo',
        field: 'charge_type',
        type: 'text'
      },
      {
        display: '',
        field: '',
        type: 'inline-button',
        options: [
          {
            cssClass: 'btn btn-success',
            icon: 'fa fa-list fa-fw m-r-sm',
            name: 'Devoluciones',
            event: 'client.viewRefunds',
            conditionality: 'this.data.refunds.length > 0'
          },
          {
            cssClass: 'btn btn-primary',
            icon: 'fa fa-reply fa-fw m-r-sm',
            name: 'Efectuar devolución',
            event: 'client.makeRefund',
            conditionality: 'this.data.paid_at !== null'
          }
        ]

      }
    ],
    filters: [{}]
  };

  broadcast$: Subscription;

  constructor(
    public activeModal: NgbActiveModal,
    private readonly chargeService: ChargesService,
    private readonly broadcast: BroadcastService,
    private readonly appModal: ModalComponent
  ) { }

  ngOnInit(): void {
    this.broadcast$ = this.broadcast.events.subscribe(event => {
      switch (event.name) {
        case 'client.makeRefund': this.makeRefundItem(event.data); break;
        case 'client.viewRefunds': this.viewRefundsItem(event.data); break;
      }
    });
    this.dataTableConfig.config.params.id_clients = this.data.id_clients;
  }

  ngOnDestroy(): void {
    this.broadcast$.unsubscribe();
  }

  makeRefundItem(data): void {
    const props: SharedComponent = new SharedComponent(MakeRefundComponent, data, { title: 'Devolución parcial o total' });
    this.appModal.open(props);
  }

  viewRefundsItem(data): void {
    const props: SharedComponent = new SharedComponent(
      ReviewRefundsComponent,
      data,
      { title: `Devoluciónes del cliente ${this.data.name}` }
    );
    this.appModal.open(props);
  }

}
