import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { environment } from '../../../../../environments/environment';
import { ClientsService } from '../../../../services/clients.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FromService } from '../../../../providers/form.service';
import { SwalService } from '../../../../services/swal.service';
import { BroadcastService } from '../../../../services/broadcast.service';
@Component({
  selector: 'app-offline-payment',
  templateUrl: './offline-payment.component.html',
  styleUrls: ['./offline-payment.component.scss']
})
export class OfflinePaymentComponent implements OnInit {
  @Input() data: any;
  client: any;
  offline_form: FormGroup;
  months_ahead = environment.months_ahead;
  payment_types = environment.payment_types;

  form: FormGroup = this.formBuilder.group({
    months_ahead: ['', Validators.required],
    payment_types: ['', Validators.required],
    id_clients: ['']
  });
  constructor(
    public activeModal: NgbActiveModal,
    public modal: NgbModal,
    private readonly clientService: ClientsService,
    private readonly formBuilder: FormBuilder,
    private readonly fromService: FromService,
    private readonly broadcast: BroadcastService,
    private readonly swal: SwalService
  ) { }

  ngOnInit(): void {
    this.clientService.show(this.data.client.id_clients).subscribe((data: any) => {
      this.client = data.response;

      if (this.client.debt > 0) {
        this.form.removeControl('months_ahead');
      }
    });
    this.fromService.setForm(this.form);
  }

  offlinePayment(): void {
    this.form.controls.id_clients.setValue(this.client.id_clients);
    if (this.form.valid) {

      if (this.data.firstCharge) {
        this.clientService.subscribeToPlanOffline(this.client.id_clients, this.form.value).subscribe((resp: any) => {
          if (resp.success) {
            const reference = resp.response.data.charges.data[0].payment_method.reference;
            this.swal.success({ title: 'Referencia generada exitosamente', text: reference }).then(() => {
              this.modal.dismissAll();
              // this.broadcast.reloadDataTable();
            });
          } else {
            this.swal.error({ title: 'Ocurrio un error al momento de generar el pago' });
          }
        });
      } else {
        this.clientService.createOfflineCharge(this.form.value).subscribe((resp: any) => {
          if (resp.success) {
            const payment_type = this.form.get('payment_types').value;
            let reference = '';
            let title_offline =  '';
            switch (payment_type) {
              case 'OXXO':
                reference = resp.response.data.charges.data[0].payment_method.reference;
                title_offline = 'Referencia generada exitosamente';
                break;
              case 'SPEI':
                reference = resp.response.data.charges.data[0].payment_method.clabe;
                title_offline = 'CLABE generada exitosamente';
                break;
            }
            this.swal.success({ title: title_offline, text: reference }).then(() => {
              this.activeModal.dismiss();
              this.broadcast.reloadDataTable();
            });
          } else {
            this.swal.error({ title: 'Ocurrio un error al momento de generar el pago' });
          }
        });
      }
    }

  }

  totalOffline(): number {
    const contract_total = parseFloat(this.data.contract.total);
    const sr = parseFloat(this.data.contract.sr);
    const monthly_fee = parseInt(this.data.contract.monthly_fee, 10) + sr;
    const months = this.form.get('months_ahead').value;

    if (this.data.firstCharge) {

      // const total = (monthly_fee * months) + (contract_total + 100);
      const total = (monthly_fee * months) + (contract_total + 0);

      return total;
    }
    if (!this.data.firstCharge) {
      // const total = (monthly_fee * months) + 100;
      const total = (monthly_fee * months) + 0;
      // const total = this.data.contract.total * months;

      return total;
    }
  }
}
