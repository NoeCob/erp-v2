import { Component, OnInit, Input } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { FromService } from '../../../providers/form.service';
import { ClientsService } from '../../../services/clients.service';
import { SwalService } from '../../../services/swal.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BroadcastService } from '../../../services/broadcast.service';
import { environment } from '../../../../environments/environment';
import { VendorsService } from '../../../services/vendors.service';
import { GroupsService } from '../../../services/groups.service';
export class InvoiceData {
  invoice_data = {
    name: [],
    rfc: [],
    address: [],
    outdoor_number: [],
    inside_number: [],
    phone: [],
    email: [],
    between_streets: [],
    colony: [],
    postal_code: [],
    state: [],
    county: []
  };
}
export class CardData {
  cards = {
    id_cards: [],
    name: [],
    address: [],
    outdoor_number: [],
    inside_number: [],
    phone: [],
    email: [],
    between_streets: [],
    colony: [],
    postal_code: [],
    state: [],
    county: [],
    country: [],
    card_number: [],
    exp_month: [],
    exp_year: [],
    default_card: []
  };
}
@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.scss']
})
export class ClientEditComponent implements OnInit {
  @Input() data: any;
  baseUrl = `${environment.apiUrl}`;
  // vendors = environment.vendors;
  client: any;
  vendors = [];
  default_vendor: any;
  group: any; /**Temporal*/
  groups = [];
  documents = {
    id: '',
    id_reverse: null,
    proof_of_address: null,
    profile: null
  };
  id: any;
  id_back: any;
  voucher: any;
  profile: any;
  form: FormGroup = this.formBuilder.group({
    name: ['', Validators.required],
    rfc: [''],
    address: ['', Validators.required],
    outdoor_number: ['', Validators.required],
    inside_number: [''],
    phone: ['', Validators.required],
    email: ['', Validators.required],
    between_streets: ['', Validators.required],
    colony: ['', Validators.required],
    postal_code: ['', Validators.required],
    state: ['', Validators.required],
    county: ['', Validators.required],
    vendor: [''],
    id_groups: [''],
    coordinate: this.formBuilder.group({ latitude: ['', Validators.required], longitude: ['', Validators.required] }),
    images: this.formBuilder.group({
      id: [''],
      id_reverse: [''],
      proof_of_address: [''],
      profile: ['']
    }),
    extra_data: [''],
    cards: new FormArray([])
  });

  constructor(
    private readonly broadcast: BroadcastService,
    private readonly formBuilder: FormBuilder,
    private readonly fromService: FromService,
    public activeModal: NgbActiveModal,
    private readonly clientService: ClientsService,
    private readonly groupService: GroupsService,
    private readonly vendorsService: VendorsService,
    private readonly swal: SwalService) { }

  ngOnInit(): void {
    this.clientService.show(this.data.id_clients).subscribe((data: any) => {
      this.client = data.response;
      this.group = 'Selecciona el grupo'; /**Temporal */
      this.getGroups();
      this.getVendors(this.client.id_clients);
      for (const vendor of this.vendors) {
        vendor.token = 'OPENPAY';
        if (vendor.id_vendor === 1) {
          vendor.token = 'CONEKTA';
        }
      }

      if (this.client.coordinate === '' || this.client.coordinate === null) {
        this.client.coordinate = { latitude: '', longitude: '' };
      }
      Object.keys(this.client).forEach(key => {
        if (['cards', 'invoice_data', 'coordinate.latitude', 'coordinate.longitude'].indexOf(key) === -1 && this.form.controls[key]) {
          this.form.controls[key].setValue(this.client[key]);
        }
      });

      const grp = [];
      const inD = new InvoiceData();
      Object.keys(inD.invoice_data).forEach(key => {
        const value = (this.client.invoice_data !== null && key in this.client.invoice_data) ? this.client.invoice_data[key] : '';
        grp[key] = [value, inD.invoice_data[key]];
      });

      this.form.addControl('invoice_data', this.formBuilder.group(grp));
      this.updateInvoiceDataValidators();

      if (this.client.cards) {
        const cardData = this.form.get('cards') as FormArray;
        for (let i = 0; i < this.client.cards.length; i++) {
          const grpp = [];
          const cardD = new CardData();
          const card = this.client.cards[i];
          Object.keys(cardD.cards).forEach(key => {
            grpp[key] = [card[key], cardD.cards[key]];
          });
          cardData.push(this.formBuilder.group(grpp));
        }
      }

      this.clientService.getImages(this.client.id_clients).subscribe((resp: any) => {
        this.documents.id = this.getImage('id.', resp.response.documents);
        this.documents.id_reverse = this.getImage('id_reverse.', resp.response.documents);
        this.documents.proof_of_address = this.getImage('proof_of_address.', resp.response.documents);
        this.documents.profile = this.getImage('profile.', resp.response.documents);
      });

    });
    this.fromService.setForm(this.form);
  }

  getVendors(id_clients): void {
    this.vendorsService.show(id_clients).subscribe((resp: any) => {
      // this.vendors = resp.response;
      this.vendors = resp.response || [];
      this.default_vendor = '-Selecciona el vendor-';
      for (const vendor of this.vendors) {
        if (vendor.default === 1) {
          this.default_vendor = vendor.name;
        }
      }
    });
  }

  getGroups(): void {
    this.groupService.getGroups().subscribe((resp: any) => {
      resp.data.forEach(group => {
        const groupObject = { id: group.id_groups, name: group.name };
        this.groups.push(groupObject);
      });
    });
  }

  updateClient(): void {
    if (this.form.valid) {
      const params = this.form.value;
      if (params.invoice_data.rfc === '' || params.invoice_data.email === '') {
        params.invoice_data = '';
      }
      this.swal.warning({ title: '¿Esta seguro de querer actualizar los datos del cliente?' }).then(result => {
        if (result.value) {
          this.clientService.update(this.client.id_clients, params).subscribe((data: any) => {
            if (data.success) {
              this.swal.success().then(() => {
                this.activeModal.dismiss();
                this.broadcast.reloadDataTable();
              });
            } else {
              this.swal.error();
            }
          });
        }
      });
    }
  }
  
  updateInvoiceDataValidators(): void {

    const invoiceName = this.form.get('invoice_data').get('name');
    const invoiceRfc = this.form.get('invoice_data').get('rfc');
    const invoiceEmail = this.form.get('invoice_data').get('email');
    const rfcClient = this.form.get('rfc');

    // this.form.valueChanges.subscribe(form => {
    //   if (form.rfc !== null) {
    //     rfcClient.setValidators([Validators.required]);
    //     invoiceName.setValidators([Validators.required]);
    //     invoiceRfc.setValidators([Validators.required]);
    //     invoiceEmail.setValidators([Validators.required]);
    //   }
    //   rfcClient.updateValueAndValidity({ onlySelf: true });
    //   invoiceName.updateValueAndValidity({ onlySelf: true });
    //   invoiceRfc.updateValueAndValidity({ onlySelf: true });
    //   invoiceEmail.updateValueAndValidity({ onlySelf: true });
    //   console.log(form);
    //   this.form.setErrors({ invalid: true });
    //   if (form.invoice_data.name !== '' || form.rfc !== '' || form.invoice_data.rfc !== '' || form.invoice_data.email !== '') {
    //     this.form.setErrors({ invalid: false });
    //   }
    //   console.log(this.form);
    // });

    this.form.get('invoice_data').valueChanges.subscribe(invoice_data => {
      if (invoice_data.name !== '' || invoice_data.name !== null || invoice_data.rfc !== '' || invoice_data.rfc !== null
        || invoice_data.email !== '' || invoice_data.email !== null) {
        rfcClient.setValidators([Validators.required]);
        invoiceName.setValidators([Validators.required]);
        invoiceRfc.setValidators([Validators.required]);
        invoiceEmail.setValidators([Validators.required]);
      }
      rfcClient.updateValueAndValidity({ onlySelf: true });
      invoiceName.updateValueAndValidity({ onlySelf: true });
      invoiceRfc.updateValueAndValidity({ onlySelf: true });
      invoiceEmail.updateValueAndValidity({ onlySelf: true });
    });
  }

  private getImage(type, images): any {
    let url = null;
    images.forEach(image => {
      if (image.indexOf(type) > -1) {
        url = `${this.baseUrl}/documents/contracts/${image}`;
      }
    });

    return url;
  }

  get cards(): FormArray { return this.form.get('cards') as FormArray; }

}
