import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthGuardService } from '../services/auth-guard.service';
import { DefaultLayoutComponent } from '../containers/default-layout/default-layout.component';
import { ClientsComponent } from './clients/clients.component';
import { RoleGuardService } from '../services/role-guard.service';

export const AdminRoutes: Routes = [
    {
        path: 'admin',
        redirectTo: 'admin/inicio',
        pathMatch: 'full'
    },
    {
        path: 'admin',
        component: DefaultLayoutComponent,
        canActivate: [AuthGuardService, RoleGuardService],
        data: {
            expectedRoles: [{role: 'Administrator'}, {role: 'Technician'}]
        },
        children: [
            {
                path: 'inicio',
                component: DashboardComponent,
                canActivate: [RoleGuardService],
                data: {
                    expectedRoles: [{role: 'Administrator'}]
                }
            },
            {
                path: 'clientes',
                component: ClientsComponent,
                canActivate: [RoleGuardService],
                data: {
                    expectedRoles: [{role: 'Administrator'}]
                }
            },
            {
                path: 'administracion',
                loadChildren: './administration/administration.module#AdministrationModule',
                canActivate: [RoleGuardService],
                data: {
                    expectedRoles: [{role: 'Administrator'}]
                }
            },
            {
                path: 'servicio',
                loadChildren: './service/service.module#ServiceModule',
                canActivate: [RoleGuardService],
                data: {
                    expectedRoles: [{role: 'Administrator'}, {role: 'Technician'}]
                }
            },
            {
                path: 'social',
                loadChildren: './social/social.module#SocialModule',
                canActivate: [RoleGuardService],
                data: {
                    expectedRoles: [{role: 'Administrator'}]
                }
            },
            {
                path: 'finanzas',
                loadChildren: './finances/finances.module#FinancesModule',
                canActivate: [RoleGuardService],
                data: {
                    expectedRoles: [{role: 'Administrator'}]
                }
            },
            {
                path: 'vendedores',
                loadChildren: './sellers/sellers.module#SellersModule',
                canActivate: [RoleGuardService],
                data: {
                    expectedRoles: [{role: 'Administrator'}]
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(AdminRoutes)
    ]
})

export class AdminModule { }
