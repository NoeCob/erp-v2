import { Routes } from '@angular/router';
import { AdministratorsComponent } from './administrators/administrators.component';
import { AppUsageComponent } from './app-usage/app-usage.component';

export const AdministrationRoutes: Routes = [
    {
        path: 'administradores',
        data: {role: 'Administrator', name: 'Administrador'},
        component: AdministratorsComponent
    }, 
    {
        path: 'tecnicos',
        data: {role: 'Technician', name: 'Técnico'},
        component: AdministratorsComponent
    },
    {
        path: 'app-usage',
        component: AppUsageComponent
    }
];
