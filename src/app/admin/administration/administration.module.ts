import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from '../../components/components.module';
import { AdministrationRoutes } from './administration.routing';
import { AdministratorsComponent } from './administrators/administrators.component';
import { AppUsageComponent } from './app-usage/app-usage.component';

@NgModule({
  declarations: [
    AdministratorsComponent,
    AppUsageComponent
  ],
  exports: [AdministratorsComponent],
  imports: [
    CommonModule,
    ComponentsModule,
    RouterModule.forChild(AdministrationRoutes)
  ]
})
export class AdministrationModule { }
