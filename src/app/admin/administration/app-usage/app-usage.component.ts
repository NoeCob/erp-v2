import { Component, OnInit } from '@angular/core';
import { AppUsageService } from '../../../services/app-usage.service';

@Component({
  selector: 'app-app-usage',
  templateUrl: './app-usage.component.html',
  styleUrls: ['./app-usage.component.scss']
})
export class AppUsageComponent implements OnInit {
  app_data: any;
  constructor(private readonly appUsageService: AppUsageService) { }

  ngOnInit(): void {
    this.getAppUsageData();
  }

  getAppUsageData(): void {
    this.appUsageService.show().subscribe((resp: any) => {
      this.app_data = resp;
    });
  }
}
