import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FromService } from '../../../../providers/form.service';
import { SwalService } from '../../../../services/swal.service';
import { GroupsService } from '../../../../services/groups.service';
import { BroadcastService } from '../../../../services/broadcast.service';

@Component({
  selector: 'app-create-edit-group',
  templateUrl: './create-edit-group.component.html',
  styleUrls: ['./create-edit-group.component.scss']
})
export class CreateEditGroupComponent implements OnInit {
  @Input() data: any;
  group: any;
  groups = [];
  signIntoData = {
    name: '',
    created_at: '',
    deposit: 0,
    installation_fee: 0,
    monthly_fee: 0
  };
  form: FormGroup = this.formBuilder.group({
    name: [''],
    deposit: ['', Validators.required],
    installation_fee: ['', Validators.required],
    trial_days: [''],
    trial_days_price: [''],
    monthly_fee: ['', Validators.required],
    sign_into: ['']
  });

  constructor(
    public activeModal: NgbActiveModal,
    private readonly groupService: GroupsService,
    private readonly broadcast: BroadcastService,
    private readonly formBuilder: FormBuilder,
    private readonly fromService: FromService,
    private readonly swal: SwalService
  ) { }

  ngOnInit(): void {
    this.getGroups();
    this.getCurrentGroupInfo();
    this.getSignIntoInfo();
    if (this.data.status === 'edit') {
      this.groupService.show(this.data.editData.id_groups).subscribe((resp: any) => {
        this.group = resp.response;
        Object.keys(this.group).forEach(key => {
          if (this.group.hasOwnProperty(key) && !!this.form.controls[key]) {
            if (key === 'monthly_fee' || key === 'deposit' || key === 'trial_days_price' || key === 'installation_fee') {
              this.form.controls[key].setValue(Number((this.group[key]) / 100));
            } else {
              this.form.controls[key].setValue(this.group[key]);
            }

          }
        });
      });
    }
    this.fromService.setForm(this.form);
  }

  save(): void {
    if (this.data.status === 'edit') {
      this.updateGroup();
    }
    if (this.data.status === 'create') {
      this.saveGroup();
    }
  }

  saveGroup(): void {
    this.prices();
    this.removeSignInto();
    if (this.form.valid) {
      this.swal.warning({ title: '¿Esta seguro de querer guardar los datos del grupo?' }).then(result => {
        if (result.value) {
          this.groupService.create(this.form.value).subscribe((resp: any) => {
            if (resp.success) {
              this.swal.success().then(() => {
                this.activeModal.dismiss();
                this.broadcast.reloadDataTable();
              });
            } else {
              this.swal.error({ title: 'Ocurió un error al guardar los datos' });
            }
          });
        }
      });
    }
  }

  updateGroup(): void {
    this.prices();
    this.removeSignInto();
    if (this.form.valid) {
      this.swal.warning({ title: '¿Esta seguro de querer actualizar los datos del grupo?' }).then(result => {
        if (result.value) {
          this.groupService.update(this.data.editData.id_groups, this.form.value).subscribe((resp: any) => {
            if (resp.success) {
              this.swal.success().then(() => {
                this.activeModal.dismiss();
                this.broadcast.reloadDataTable();
              });
            } else {
              this.swal.error({ title: 'Ocurió un error al actualizar los datos' });
            }
          });
        }
      });
    }
  }

  getGroups(): void {
    this.groupService.getGroups().subscribe((resp: any) => {
      this.groups.push({ id: '', name: '' });
      resp.data.forEach(group => {
        const groupObject = { id: group.id_groups, name: group.name };
        this.groups.push(groupObject);
      });
    });
  }

  getSignIntoInfo(): void {

    this.form.get('sign_into').valueChanges.subscribe(field_value => {
      const sign_into_id = field_value;

      if (sign_into_id !== '') {
        this.groupService.show(sign_into_id).subscribe((resp: any) => {
          this.signIntoData.name = resp.response.name;
          this.signIntoData.deposit = (resp.response.deposit) / 100;
          this.signIntoData.installation_fee = (resp.response.installation_fee) / 100;
          this.signIntoData.monthly_fee = (resp.response.monthly_fee) / 100;
          this.signIntoData.created_at = resp.response.created_at;
        });
      } else {
        this.getCurrentGroupInfo();
      }
    });
  }

  removeSignInto(): void {
    if (this.form.get('sign_into').value === '') {
      this.form.removeControl('sign_into');
    }
  }

  getCurrentGroupInfo(): void {
    this.form.valueChanges.subscribe(field => {
      this.signIntoData.name = field.name;
      this.signIntoData.deposit = field.deposit;
      this.signIntoData.installation_fee = field.installation_fee;
      this.signIntoData.monthly_fee = field.monthly_fee;
      this.signIntoData.created_at = '';
    });
  }

  prices(): void {
    const deposit = Number((this.form.get('deposit').value) * 100);
    const trial_days_price = Number((this.form.get('trial_days_price').value) * 100);
    const monthly_fee = Number((this.form.get('monthly_fee').value) * 100);
    const installation_fee = Number((this.form.get('installation_fee').value) * 100);
    this.form.controls.deposit.setValue(deposit);
    this.form.controls.trial_days_price.setValue(trial_days_price);
    this.form.controls.monthly_fee.setValue(monthly_fee);
    this.form.controls.installation_fee.setValue(installation_fee);
  }
}
