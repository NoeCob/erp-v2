import { Component, OnInit, Input } from '@angular/core';
import { CommissionsService } from '../../../../services/commissions.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-client-invalid',
  templateUrl: './client-invalid.component.html',
  styleUrls: ['./client-invalid.component.scss']
})
export class ClientInvalidComponent implements OnInit {
  @Input() data: any;
  declined: any;
  constructor(
    private readonly commissionService: CommissionsService,
    public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    this.commissionService.getDeclined(this.data.id_commissions).subscribe((resp: any) => {
      this.declined = resp;
    });
  }

}
