// Angular
import { NgModule, LOCALE_ID  } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
// import { registerLocaleData } from '@angular/common';
// import localeEs from '@angular/common/locales/es';
// import localeEsMX from '@angular/common/locales/es-MX';
// 3rd Party Modules
import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule
  // SidebarNavHelper
} from '@coreui/angular';
import {
  PerfectScrollbarModule,
  PERFECT_SCROLLBAR_CONFIG,
  PerfectScrollbarConfigInterface
} from 'ngx-perfect-scrollbar';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { NgxLoadingModule } from 'ngx-loading';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
// App own Modules
import { SessionModule } from './session/session.module';
import { AdminModule } from './admin/admin.module';
// Components
import { DefaultLayoutComponent } from './containers/default-layout/default-layout.component';
import { AppComponent } from './app.component';
import { routes } from './app.routing';
import * as moment from 'moment';
// Interceptors
import { CatchAllInterceptorService } from './providers/catch-all-intercerptor.service';
import { RefreshTokenInterceptorService } from './providers/refresh-token-interceptor.service';

// registerLocaleData(localeEs, 'es-MX');

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

const APP_CONTAINERS = [
  DefaultLayoutComponent,
  AppComponent
];

@NgModule({
  imports: [
    AdminModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    BrowserModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    ChartsModule,
    HttpClientModule,
    LoadingBarHttpClientModule,
    LoadingBarRouterModule,
    LoadingBarModule,
    NgbModule,
    NgxLoadingModule.forRoot({
      animationType: 'sk-circle',
      primaryColour: '#20a8d8',
      secondaryColour: '#dddddd',
      backdropBackgroundColour: 'rgba(255, 255, 255, 0.5)',
      backdropBorderRadius: '5px'
    }),
    PerfectScrollbarModule,
    RouterModule.forRoot(routes),
    SessionModule,
    TabsModule.forRoot()
  ],
  declarations: [...APP_CONTAINERS],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CatchAllInterceptorService,
      multi: true
    }
    // {
    //   provide: LOCALE_ID, useValue: 'es-MX'
    // }
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
  constructor() {
    moment.defineLocale('es-mx', {
      parentLocale: 'es',
      months : 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
      monthsShort : 'Ene_Feb_Mar_Abr_May_Jun_Jul_Ago_Sep_Oct_Nov_Dic.'.split('_'),
      monthsParseExact : true,
      weekdays : 'domingo_lunes_martes_miércoles_jueves_viernes_sábado'.split('_'),
      weekdaysShort : 'dom._lun._mar._mié._jue._vie._sáb.'.split('_'),
      weekdaysMin : 'do_lu_ma_mi_ju_vi_sá'.split('_'),
      weekdaysParseExact : true,
      dayOfMonthOrdinalParse: /\d{1,2}/,
      ordinal(dayNumber: any): any {
        return dayNumber;
      }
    });
    moment.locale('es-mx');
  }
}
