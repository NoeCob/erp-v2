import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { AuthorizationService } from './authorization.service';
import * as JWT from 'jwt-decode';

@Injectable({
    providedIn: 'root'
})

export class RoleGuardService implements CanActivate {

    tokenPayload = {
        user: {
            roles: []
        }
    };

    constructor(public auth: AuthorizationService, public router: Router) { }

    canActivate(route: ActivatedRouteSnapshot): boolean {

        let authorized = false;
        const expectedRoles = route.data.expectedRoles;
        this.tokenPayload = this.decodedToken;

        if (this.auth.isAuthenticated()) {
            expectedRoles.forEach(expectedRole => {
                this.tokenPayload.user.roles.forEach(userRole => {

                    if (userRole.name === expectedRole.role) {
                        authorized = true;

                        return authorized;
                    }
                });
            });

            return authorized;
        }
    }

    get decodedToken(): any {
        const token = window.localStorage.getItem('access_token');
        const decoded = JWT(token);

        return decoded;
    }
}
