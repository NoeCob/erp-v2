import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})

export class AppUsageService {
    baseUrl = `${environment.apiUrl}`;
    constructor(private readonly http: HttpClient) { }
    
    show(): any {
        return this.http.get(`${this.baseUrl}/appUsage`);
    }
}
