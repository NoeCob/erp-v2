import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError, flatMap } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { SwalService } from '../services/swal.service';

@Injectable({
  providedIn: 'root'
})

export class RefreshTokenInterceptorService implements HttpInterceptor {

  constructor(
    private readonly router: Router,
    private readonly injector: Injector,
    private readonly swal: SwalService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      catchError((err: HttpErrorResponse) => {

        console.log('checando los errores');
        console.log(err);

        if (err.status === 401 && err.error === 'token_expired') {
          const http = this.injector.get(HttpClient);
          const token: string = localStorage.getItem('access_token');

          return http.post<any>(`${environment.apiUrl}/auth/refresh`,
            {},
            {
              headers: {
                Authorization: `Bearer ${token}`
              }
            })
            .pipe(
              flatMap(data => {
                localStorage.setItem('access_token', data.token);
                const cloneRequest = req.clone({
                  setHeaders: {
                    Authorization: `Bearer ${data.token}`
                  }
                });

                return next.handle(cloneRequest);
              })
            );
          // tslint:disable-next-line: unnecessary-else
        } else {
          if (err.error === 'token_invalid' || err.error === 'token_error' || err.error.error === 'user_not_found') {
            localStorage.removeItem('access_token');
            localStorage.removeItem('user');
            this.swal.warning({ title: 'Sesion expirada', text: '', showCancelButton: false, confirmButtonText: 'Ok' });
            this.router.navigateByUrl('/login');
          }
        }

        return throwError(err);
      })
    );
  }
}
