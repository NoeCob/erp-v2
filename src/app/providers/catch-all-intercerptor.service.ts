import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, flatMap } from 'rxjs/operators';
import { SwalService } from '../services/swal.service';
import { AuthorizationService } from '../services/authorization.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class CatchAllInterceptorService implements HttpInterceptor {

  constructor(
    private readonly router: Router,
    private readonly injector: Injector,
    public authService: AuthorizationService,
    public swal: SwalService
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(req).pipe(
      catchError(httpError => {
        let errorMessage: any;
        let displayMessage: any;
        if (httpError instanceof ErrorEvent) {
          // client-side error
          errorMessage = `Client-side error: ${httpError.error.message}`;
          this.swal.error({ text: errorMessage }).catch();
        } else {
          if (httpError.error.error === 'user_not_found' || httpError.error === 'token_invalid' || httpError.error === 'token_error') {
            errorMessage = 'Sesion expirada';
            this.authService.logout();
            this.swal.warning({ title: 'Sesion expirada', text: '', showCancelButton: false, confirmButtonText: 'Ok' });
            this.router.navigateByUrl('/login');
          } else {

            if (httpError.status === 401 && httpError.error === 'token_expired') {
              const http = this.injector.get(HttpClient);
              const token: string = localStorage.getItem('access_token');

              return http.post<any>(`${environment.apiUrl}/auth/refresh`,
                {},
                {
                  headers: {
                    Authorization: `Bearer ${token}`
                  }
                })
                .pipe(
                  flatMap(data => {
                    localStorage.setItem('access_token', data.token);
                    const cloneRequest = req.clone({
                      setHeaders: {
                        Authorization: `Bearer ${data.token}`
                      }
                    });

                    return next.handle(cloneRequest);
                  })
                );
              // tslint:disable-next-line: unnecessary-else
            } else {

              // backend error
              errorMessage = `Server-side error: ${httpError.status} ${httpError.message}`;
              const errors = [
                `${httpError.status} - ${httpError.statusText}`,
                httpError.url
              ];

              displayMessage = '<div class="server-error">';
              displayMessage += '<p>Server-side error:</p>';
              displayMessage += `<ul>${this.returnFormated(errors)}</ul>`;
              displayMessage += '</div>';

              switch (httpError.status) {
                case 401:
                  if (httpError.error === 'bad_credentials') {
                    this.swal.error({
                      title: 'Credenciales invalidas',
                      text: '',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    });
                    break;
                  }
                  errorMessage = httpError;
                  break;
                case 402:
                  const failure_messages = httpError.error.response.errors || [];
                  displayMessage = '<div class="server-error">';
                  // displayMessage += '<p>Vendor error:</p>';
                  let errorsShow = '<li>Error al procesar el pago.</li>';
                  if (failure_messages.length > 0) {
                    const formatedErrors = this.returnFormated(failure_messages);
                    if (formatedErrors !== '') {
                      errorsShow = formatedErrors;
                    }
                  }
                  displayMessage += `<ul>${errorsShow}</ul>`;
                  displayMessage += '</div>';
                  this.swal.error({ html: displayMessage });
                  
                  break;
                default:
                  this.authService.logout();
                  this.router.navigateByUrl('/login');
                  this.swal.error({ html: displayMessage }).catch();
                  break;
              }
            }

          }
        }

        // aquí podrías agregar código que muestre el error en alguna parte fija de la pantalla.
        // this.errorService.show(errorMessage);
        // console.warn(errorMessage);
        return throwError(errorMessage);
      })
    );
  }

  returnFormated(errors: Array<any>): string {
    let text = '';
    errors.forEach(error => {
      if (error.hasOwnProperty('failure_message')) {
        text += `<li>${error.failure_message}</li>`;
      } else {
        text += `<li>${error}</li>`;
      }
    });

    return text;
  }

  // returnPaymentErrors(errors: Array<any>): string {
  //   let text = '';
  //   errors.forEach(element => {
  //     text += `<li>${element.failure_message}</li>`;
  //   });

  //   return text;
  // }
}
