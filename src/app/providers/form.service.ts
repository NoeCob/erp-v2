import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';

@Injectable()
export class FromService {
  form: FormGroup;
  $form = new Subject();

  setForm(form: FormGroup): void {
    this.$form.next(form);
    this.form = form;
  }

  getForm(): FormGroup {
    return this.form;
  }
}
