import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-client-history-change',
  templateUrl: './client-history-change.component.html',
  styleUrls: ['./client-history-change.component.scss']
})
export class ClientHistoryChangeComponent implements OnInit {
  @Input('item') item: any;
  before_data: any;
  after_data: any;
  
  ngOnInit(): void {
    this.before_data = JSON.parse(this.item.element.before_data);
    this.after_data = JSON.parse(this.item.element.after_data);
  }

}
