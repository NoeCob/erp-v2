import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-completions',
  templateUrl: './completions.component.html',
  styleUrls: ['./completions.component.scss']
})
export class CompletionsComponent implements OnInit {
  @Input('item') item: any;

  // tslint:disable-next-line: typedef
  ngOnInit() {
    // tslint:disable-next-line: no-empty
  }

}
