import { Component, OnInit, Input } from '@angular/core';
import { BroadcastService } from '../../services/broadcast.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {
  @Input('type') type: any;
  @Input('items') items: any;
  
  constructor(private readonly broadcast: BroadcastService) { }

  ngOnInit(): void {
    //
  }
}
