import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FromService } from '../../../providers/form.service';
import { DomSanitizer } from '@angular/platform-browser';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss']
})
export class FormFieldComponent implements OnInit {
  @Input() type: string;
  @Input() options: any;

  field: FormControl;
  private image: any;

  constructor(
    private readonly fromService: FromService,
    private readonly sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.field = this.form.get(this.options.path) as FormControl;
  }

  get form(): FormGroup {
    return this.fromService.form;
  }

  get isChecked(): boolean {
    return this.field.value === (this.options.hasOwnProperty('trueValue') ? this.options.trueValue : true);
  }

  get checked(): boolean {
    if (this.options.default === 1 || this.options.default === true) {
      return true;
    }

    return false;
  }

  reset(event): void {
    const cards = this.fromService.getForm().get('cards');
    const controls = 'controls';
    if (event.target.value === 'on') {
      cards[controls].forEach(card => {
        card.get('default_card').setValue(0);
        if (card.value.id_cards === Number(event.target.id)) {
          card.get('default_card').setValue(true);
        }
      });
    }
    this.field.markAsTouched();
    this.field.markAsDirty();
  }

  bgImage(): String {
    return this.image || this.options.image || 'http://cliquecities.com/assets/no-image-e3699ae23f866f6cbdf8ba2443ee5c4e.jpg';
  }

  bgStyle(): any {
    const imageString = JSON.stringify(this.bgImage()).replace(/\\n/g, '');
    const style = `url(${imageString})`;

    return this.sanitizer.bypassSecurityTrustStyle(style);
    // return 'url(' + this.bgImage + ');';
  }

  switched(e): void {
    const target = e.target;
    this.field.setValue(target.checked ?
      (this.options.hasOwnProperty('trueValue') ? this.options.trueValue : true) :
      (this.options.hasOwnProperty('falseValue') ? this.options.falseValue : false));

    this.field.markAsTouched();
    this.field.markAsDirty();
  }
    
  readFile(e): void {
    const input = e.target;
    if (input.files && input.files[0]) {
      const reader: FileReader = new FileReader();
      reader.onload = (ev: any) => {
        this.image = ev.target.result;
        this.field.setValue(ev.target.result);
      };
      reader.readAsDataURL(input.files[0]);
      this.field.markAsTouched();
      this.field.markAsDirty();
    }
  }

  // search = (text$: Observable<string>) =>
  // text$.pipe(
  //   debounceTime(200),
  //   distinctUntilChanged(),
  //   map(term => term.length < 2 ? []
  //     : this.options.elements.name.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
  // );

}
