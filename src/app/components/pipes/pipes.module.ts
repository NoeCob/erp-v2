import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetFieldPipe } from './get-field.pipe';
import { CustomDatePipe } from './custom-date.pipe';

const pipes = [
  GetFieldPipe
];
@NgModule({
  declarations: [...pipes, CustomDatePipe],
  imports: [
    CommonModule
  ],
  exports: [...pipes, CustomDatePipe]
})
export class PipesModule { }
