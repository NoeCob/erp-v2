import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'customDate'
})
export class CustomDatePipe implements PipeTransform {

  transform(date: string, format: string): any {
    const myDate = moment(date);
    
    return myDate.isValid() ? myDate.format(format) : date;
  }

}
