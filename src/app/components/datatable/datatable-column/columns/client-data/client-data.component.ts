import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-client-data',
  templateUrl: './client-data.component.html',
  styleUrls: ['./client-data.component.scss']
})
export class ClientDataComponent implements OnInit {
  @Input() data: any;

  ngOnInit(): void {
    //
  }

}
