import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-debt-total',
  templateUrl: './debt-total.component.html',
  styleUrls: ['./debt-total.component.scss']
})
export class DebtTotalComponent implements OnInit {
  @Input() data: any;
  
  num = 0;

  ngOnInit(): void {
    this.num = (Number(this.data.amount) + Number(this.data.collection_fees) + Number(this.data.moratory_fees)) / 100 ;
  }

}
