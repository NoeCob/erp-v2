import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { BroadcastService } from '../../../../../services/broadcast.service';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss']
})
export class FiltersComponent implements OnInit {
    @Input() data: any;
    @ViewChild(DataTableDirective, {static: false}) datatableElement: DataTableDirective;

    selected;

    constructor(private readonly broadcast: BroadcastService) {}

    ngOnInit(): void { 
        //
    }

    isVisible(active): string {
        // tslint:disable-next-line: no-eval
        return eval(active);
    }

    radioChange(): any {
        this.broadcast.fire({
            name: 'datatable-filter', 
            data: { 
                value: this.selected, 
                type: this.data.type
            }
        });
    } 
}
