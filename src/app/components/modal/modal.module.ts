// Modules
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { NgxLoadingModule } from 'ngx-loading';
// Components
import { ModalComponent } from './modal.component';
import { ModalBodyComponent } from './modal-body/modal-body.component';
import { SharedComponentDirective } from '../../directives/shared-component.directive';
import { ContractComponent } from '../../admin/clients/contract/contract.component';
import { OfflinePaymentComponent } from '../../admin/clients/contract/offline-payment/offline-payment.component';
import { ClientEditComponent } from '../../admin/clients/client-edit/client-edit.component';
import { CloseComponent } from '../../admin/service/tickets/close/close.component';
// Services
import { HistoryModule } from '../history/history.module';
import { DatatableModule } from '../datatable/datatable.module';
import { FromService } from '../../providers/form.service';
import { FormModule } from '../form/form.module';
import { ClientHistoryComponent } from '../../admin/clients/client-history/client-history.component';
import { ClientChargesHistoryComponent } from '../../admin/clients/client-charges-history/client-charges-history.component';
import { ReferalsComponent } from '../../admin/clients/referals/referals.component';
import { TreeviewComponent } from '../../admin/clients/referals/treeview/treeview.component';
import { CommissionsComponent } from '../../admin/clients/referals/commissions/commissions.component';
import { MakeRefundComponent } from '../../admin/clients/client-charges-history/make-refund/make-refund.component';
import { ReviewRefundsComponent } from '../../admin/clients/client-charges-history/review-refunds/review-refunds.component';
import { RestorePasswordComponent } from '../../admin/clients/restore-password/restore-password.component';
import { CreateSupportTicketComponent } from '../../admin/clients/create-support-ticket/create-support-ticket.component';

import { ErrorCodeCreateEditComponent } from '../../admin/service/error-codes/error-code-create-edit/error-code-create-edit.component';
import { RecordComponent } from '../../admin/service/tickets/record/record.component';
import { AssignComponent } from '../../admin/service/tickets/assign/assign.component';
import { ReassingComponent } from '../../admin/service/tickets/reassing/reassing.component';
import { AdministratorCreateEditComponent } from '../../admin/administration/administrators/administrator-create-edit/administrator-create-edit.component';
import { CreateEditGroupComponent } from '../../admin/sellers/sales-group/create-edit-group/create-edit-group.component';
import { DebtsComponent } from '../../admin/finances/clients/debts/debts.component';
import { CreateEditCategoryComponent } from '../../admin/sellers/extras/create-edit-category/create-edit-category.component';
import { CreateEditElementComponent } from '../../admin/sellers/extras/create-edit-element/create-edit-element.component';
import { ElementsComponent } from '../../admin/sellers/extras/elements/elements.component';
import { ClientInvalidComponent } from '../../admin/sellers/commissions/client-invalid/client-invalid.component';
import { NewGroupComponent } from '../../admin/social/notify/new-group/new-group.component';
import { EditGroupComponent } from '../../admin/social/notify/edit-group/edit-group.component';
import { MembersGroupComponent } from '../../admin/social/notify/members-group/members-group.component';

const modalBodys = [
  ErrorCodeCreateEditComponent,
  RecordComponent,
  AssignComponent,
  ReassingComponent,
  CloseComponent,
  ContractComponent,
  OfflinePaymentComponent,
  ClientEditComponent,
  ClientChargesHistoryComponent,
  ClientHistoryComponent,
  ReferalsComponent,
  MakeRefundComponent,
  ReviewRefundsComponent,
  RestorePasswordComponent,
  CreateSupportTicketComponent,
  AdministratorCreateEditComponent,
  ReferalsComponent,
  CreateEditGroupComponent,
  DebtsComponent,
  ElementsComponent,
  CreateEditCategoryComponent,
  CreateEditElementComponent,
  ClientInvalidComponent,
  NewGroupComponent,
  EditGroupComponent,
  MembersGroupComponent,
  CommissionsComponent,
  TreeviewComponent
];

@NgModule({
  declarations: [
    ModalComponent,
    ModalBodyComponent,
    SharedComponentDirective,
    ...modalBodys
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    // DataTablesModule,
    DatatableModule,
    FormModule,
    HistoryModule,
    NgxLoadingModule
  ],
  entryComponents: [
    ModalComponent,
    ModalBodyComponent,
    ...modalBodys
  ],
  exports: [
    ModalComponent
  ],
  providers: [
    ModalComponent,
    NgbActiveModal,
    FromService
  ],
  bootstrap: []
})
export class ModalModule { }
