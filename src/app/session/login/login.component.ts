import { Router } from '@angular/router';
import { Component, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthorizationService } from '../../services/authorization.service';
import { UsersService } from '../../services/users.service';
import { FromService } from '../../providers/form.service';
import { RoleGuardService } from '../../services/role-guard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss'],
  providers: [AuthorizationService]
})
export class LoginComponent {
  loginForm: FormGroup = this.formBuilder.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
    appVersion: ['']
  });

  errors = [];
  loading = false;

  constructor(
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly authorizationService: AuthorizationService,
    private readonly roleGuardService: RoleGuardService,
    private readonly userService: UsersService,
    private readonly fromService: FromService
  ) {
    const access_token = localStorage.getItem('access_token');
    const user = localStorage.getItem('user');

    if (access_token != null && user != null) {
      this.loading = true;
      const userData = JSON.parse(user);
      this.userService.user(userData.id).subscribe((data: any) => {
        if (data.success) {
          this.router.navigate(['admin/inicio']);
        } else {
          localStorage.removeItem('access_token');
          localStorage.removeItem('user');
        }
      });
    }

  }

  ngOnInit(): void {
    this.fromService.setForm(this.loginForm);
  }

  logIn(): void {
    this.errors = [];

    if (this.loginForm.valid) {
      this.loading = true;

      this.authorizationService.login(this.loginForm.value).subscribe((data: any) => {
        localStorage.setItem('access_token', data.access_token);
        localStorage.setItem('user', JSON.stringify(data.user));

        const roles = this.roleGuardService.decodedToken.user.roles;

        roles.forEach(roleUser => {

          switch (roleUser.name) {
            case 'Technician':
              this.router.navigate(['/admin/servicio/tickets']);
              break;
            default:
              this.router.navigate(['/admin/inicio']);
              break;
          }
        });
      });
    }
  }

}
