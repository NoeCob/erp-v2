import { CoreUIPage } from './app.po';

// tslint:disable: only-arrow-functions
// tslint:disable: typedef
describe('core-ui App', function() {
  let page: CoreUIPage;

  beforeEach(() => {
    page = new CoreUIPage();
  });

  it('should display footer containing creativeLabs', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toContain('creativeLabs');
  });
});
